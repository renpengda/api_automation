package com.example.service;

import com.example.bean.TestDataBean;
import com.example.enums.FIlePathConstant;
import com.example.util.ApiRequestUtils;
import com.example.util.PropertyUtil;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author 任鹏达
 * @version 1.0
 */

@Service
public class MyService {

  PropertyUtil propertyUtils = new PropertyUtil(FIlePathConstant.API_URL);

  ApiRequestUtils apiRequestUtils = new ApiRequestUtils();

  public void loginApi(TestDataBean testDataBean){
    String url = propertyUtils.getProperValue("login.url");
    testDataBean.setUrl(System.getProperty("ip")+url);
    String body = apiRequestUtils.doGet(testDataBean);
    Map<String, Object> map = apiRequestUtils.saveLinked(body, testDataBean.getDataRelevancy(), "登陆接口");
    System.out.println(map.get("reason"));

  }

}
