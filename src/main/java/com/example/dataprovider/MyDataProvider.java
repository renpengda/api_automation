package com.example.dataprovider;

import com.example.util.ReadTestDataUtils;
import org.testng.annotations.DataProvider;
import java.util.Iterator;


/**
 * @author 任鹏达
 * @version 1.0
 */
public class MyDataProvider {

  /**
   * 获取用例数据
   * @return
   */
  @DataProvider
  public Iterator<Object[]> getCaseData(){

    Integer caseIds[] = {1,2};
    return ReadTestDataUtils.getInstance().getTestDataFromDB(caseIds,"automation_case.tb_api_pms");

  }

}
