package com.example.mapper;

import com.example.bean.TestDataBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author 任鹏达
 * @version 1.0
 */

@Mapper
public interface CaseMapper {
  @Select("select * from ${table} where case_id = ${caseId}")
  TestDataBean getCase(@Param("table") String table, @Param("caseId") int caseId);
}
