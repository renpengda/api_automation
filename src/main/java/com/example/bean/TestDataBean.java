package com.example.bean;

import lombok.Data;

/**
 * @author 任鹏达
 * @version 1.0
 */
@Data
public class TestDataBean {

  /**用例编号**/
  private int caseId;

  /**Service层方法名**/
  private String methodName;

  /**接口描述**/
  private String apiDescription;

  /**请求数据**/
  private String requestData;

  /**检查点**/
  private String checks;

  /**接口地址**/
  private String url;

  /**头信息**/
  private String head;

  /**数据关联**/
  private String dataRelevancy;

  /**辅助字段**/
  private String assist;

  @Override
  public String toString() {
    return "TestDataBean{" +
      "caseId=" + caseId +
      ", methodName='" + methodName + '\'' +
      ", apiDescription='" + apiDescription + '\'' +
      ", requestData='" + requestData + '\'' +
      ", checks='" + checks + '\'' +
      ", url='" + url + '\'' +
      ", head='" + head + '\'' +
      ", dataRelevancy='" + dataRelevancy + '\'' +
      ", assist='" + assist + '\'' +
      '}';
  }
}
