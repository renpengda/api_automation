package com.example.cases;


import com.example.ApiAutomationApplication;
import com.example.bean.TestDataBean;
import com.example.common.CaseExecuteByInvoke;
import com.example.dataprovider.MyDataProvider;
import com.example.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.*;

/**
 * @author 任鹏达
 * @version 1.0
 */

@SpringBootTest(classes = {ApiAutomationApplication.class})
@WebAppConfiguration
public class DemoTest extends AbstractTestNGSpringContextTests {

  @Autowired
  MyService myService;

  @Autowired
  CaseExecuteByInvoke caseExecuteByInvoke;

  @BeforeSuite
  public void beforeSuit(){
    System.setProperty("ip","http://apis.juhe.cn");
  }

  @Test
  @Parameters("caseId")
  public void demoTest(String caseId) {
    System.out.println("接受的xml参数为：" + caseId);
  }


  @Test(description = "测试描述专用", dataProviderClass = MyDataProvider.class, dataProvider = "getCaseData")
  public void getDBCaseData(TestDataBean testDataBean) {
    System.out.println(testDataBean);
    caseExecuteByInvoke.objectTypeCaseExecute(testDataBean,myService);

  }
}

