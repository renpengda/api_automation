package com.example.common;

import com.example.bean.TestDataBean;
import com.example.util.ReadTestDataUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * @author 任鹏达
 * @version 1.0
 */

@Component
@Slf4j
public class CaseExecuteByInvoke {

  public <T> void objectTypeCaseExecute(TestDataBean testDataBean, T t) {
    int count = 0;

    Method[] methods = t.getClass().getDeclaredMethods();

    for (Method method : methods) {
      if (method.getName().equals(testDataBean.getMethodName())) {
        try {
          method.invoke(t, testDataBean);
        } catch (Exception e) {
          if (e.getCause() != null && e.getCause() instanceof AssertionError) {
            throw (AssertionError) e.getCause();
          }
          throw (AssertionError) e.getCause();
        }
        break;
      }
    }
  }


  /**
   * 数组类型用例
   *
   * @param caseIds   用例id（数字）
   * @param tableName 数据库表
   * @param t
   * @param <T>
   */
  public <T> void ArrayTypeCaseExecute(Integer caseIds[], String tableName, T t) {

    try {
      Iterator<Object[]> testDataBean = ReadTestDataUtils.getInstance().getTestDataFromDB(caseIds, tableName);
      while (testDataBean.hasNext()) {
        Object[] next = testDataBean.next();
        for (Object o : next) {
          objectTypeCaseExecute((TestDataBean) o, t);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
