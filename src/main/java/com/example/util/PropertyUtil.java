package com.example.util;

import java.io.*;
import java.util.Map;
import java.util.Properties;

/**
 * @author 任鹏达
 * @version 1.0
 */
public class PropertyUtil {


  private Properties properties = new Properties();
  private String filePath;

  public PropertyUtil(String file) {

    filePath = file;

    properties = loadProperty();
  }

  /**
   * 随便传个值，做个区别
   *
   * @param file file
   * @param n
   */

  public PropertyUtil(String file, int n) {

    filePath = file;

    //properties = loadPropertyRoot();
  }

  /**
   * 获取配置文件key对应的值
   *
   * @param key 配置文件key
   * @return value
   */
  public String getProperValue(String key) {
    String value = null;
    if (properties.containsKey(key)) {

      try {
        value = properties.getProperty((key).trim());
      } catch (Exception e) {
        e.printStackTrace();
        CheckPointUtils.myAssertFalse(e.getMessage());
      }

    } else {
      CheckPointUtils.myAssertFalse("没有找到相应的键:[" + key + "] 请检查配置文件或测试数据");
    }
    return value;
  }

  /**
   * 判断文件是否包含key
   *
   * @param key key
   * @return boolean
   */
  public boolean isContainsKey(String key) {

    if (properties.containsKey(key)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * 自动生产属性文件及内容
   * @param propertyMap
   */
  public void setPropertyValue(Map<String, String> propertyMap) {

    FileOutputStream out = null;
    try {
    for (Map.Entry<String, String> entry : propertyMap.entrySet()) {
        out = new FileOutputStream(this.filePath);
        setProValue(entry.getKey(), entry.getValue(),out);
      }
    }catch (Exception e){
      e.printStackTrace();
    }finally {
      if (null!=out){
        try {
          out.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void setProValue(String key, String value ,OutputStream fos){

    try {
      properties.setProperty(key,value);
      properties.store(new OutputStreamWriter(fos,"UTF-8"),"Update value");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 加载配置文件属性
   * @return
   */
  public Properties loadProperty() {

    ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
    InputStream resourceAsStream = contextClassLoader.getResourceAsStream(filePath);
    BufferedReader bf = null;

    try {
      bf = new BufferedReader(new InputStreamReader(resourceAsStream, "UTF-8"));

      properties.load(bf);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (null != bf) {
        try {
          bf.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return properties;
  }
}
