package com.example.util;

import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import org.testng.Assert;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 任鹏达
 * @version 1.0
 */
@Slf4j
public class CheckPointUtils {

  /**
   * 断言 【指定字段】和【响应code码和msg】
   * @param resultBody
   * @param caseDescribe
   * @param map
   */
  public static void checkPointsAndCodeMsg(String resultBody, String caseDescribe, Map<String ,Object> map ){

    checkResponseCodeMsg(resultBody,caseDescribe);
    checkPoints(resultBody,map,caseDescribe);
  }


  /**
   * 断言指定字段
   * @param resultBody 响应报文
   * @param checkMap 要指定断言的字段，Map类型
   * @param describe 用例描述
   */
  public static void checkPoints(String resultBody,Map<String ,Object> checkMap, String describe){
    if (null !=checkMap && checkMap.size()!=0){
      List<Object> errorList = new ArrayList<>();
      List<Object> trueList = new ArrayList<>();
      for (Map.Entry<String, Object> entry: checkMap.entrySet()) {
        String value = JsonUtils.getJsonValueByJsonPath(resultBody,entry.getKey(),describe);
        Object object = entry.getValue();
        String actualValue = String.valueOf(object);
        if (value.equalsIgnoreCase(actualValue)){
          trueList.add(entry.getKey() + "=" +actualValue);
        }else {
          errorList.add(entry.getKey()+ "=" +actualValue);
        }

      }
      if (errorList.size()>0){
        myAssertFalse("【"+describe+"】接口--[响应body里字段断言失败]:期望值是: "+ errorList.toString());
      }else {
        MyLogger.myResponseLogger("【"+describe +"】接口--[响应body里字段断言成功]"+ trueList.toString());
      }
    }
  }


  /**
   * 断言响应 code 和 msg
   * @param resultBody 响应报文
   * @param caseDescribe 用例描述
   */
  public static void checkResponseCodeMsg(String resultBody,String caseDescribe){

    String code = JsonPath.read(resultBody,"$.code").toString();
    String msg = JsonPath.read(resultBody,"$.message").toString();

    if (code.equals("200") && msg.equalsIgnoreCase("success")){
      MyLogger.myResponseLogger("【"+ caseDescribe + "】接口--[ code,msg 断言正确 ]");
    }
    else {
      myAssertFalse("【"+ caseDescribe + "】接口--[ code,msg 断言失败 ]");
    }
  }


  /**
   * 失败断言信息打印
   * @param message 失败断言信息
   */
  public static void myAssertFalse(String message){
    log.error(message);
    Assert.fail(message);
  }
}
