package com.example.util;

/**
 * @author 任鹏达
 * @version 1.0
 */


import com.example.bean.TestDataBean;
import com.example.mapper.CaseMapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 读取数据库测试数据
 */
public class ReadTestDataUtils {


  CaseMapper caseMapper = SpringUtils.getBean(CaseMapper.class);

  private ReadTestDataUtils() {}

  private static ReadTestDataUtils readTestDataUtil = new ReadTestDataUtils();


  public static ReadTestDataUtils getInstance() {
    return readTestDataUtil;
  }

  /**
   * 读取数据库数据，返回list
   * @param caseIds 读取的数据id
   * @param table  表名
   * @return 返回list
   */
  private List<TestDataBean> getTestData(Integer caseIds[], String table) {

    List<TestDataBean> list = new ArrayList<>();

    TestDataBean testDataBean;

    for (int caseId : caseIds) {
      testDataBean = caseMapper.getCase(table, caseId);
      list.add(testDataBean);
    }
    return list;
  }

  /**
   * 从数据库表读取Case
   * @param caseIds 用例id
   * @param table 表名
   * @return
   */
  public Iterator<Object[]> getTestDataFromDB(Integer caseIds[], String table) {

    List<Object[]> list = new ArrayList<>();

    List<TestDataBean> testDataBeans = getTestData(caseIds, table);

    for (Object object : testDataBeans) {
      list.add(new Object[]{ object });
    }

    return list.iterator();

  }
}
