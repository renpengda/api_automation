package com.example.util;

import com.alibaba.fastjson.JSONObject;
import com.example.bean.TestDataBean;
import com.jayway.jsonpath.JsonPath;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * @author 任鹏达
 * @version 1.0
 */

public class JsonUtils {


  /**
   * Jsonpath取值
   * @param data
   *  data[0] body
   *  data[1] jsonpath
   *  data[2] 接口名称
   * @return 解析后的String
   */
  public static String getJsonValueByJsonPath(String ...data){
    Object object = null;

    try {
      object = JsonPath.read(data[0],data[1].toString());
      return String.valueOf(object);
    } catch (Exception e) {
      if (null == object || String.valueOf(object).equals("")){
        if (data.length==3){
          CheckPointUtils.myAssertFalse("["+data[2]+"--接口]"+"响应内容解析不到预期字段：" + data[1]);
        }
        else {
          CheckPointUtils.myAssertFalse("解析不到这个Json里的目标字段: "+ data[0]);
        }
      }
    }
    return "-1";
  }

  /**
   * 从body中获取数组的长度
   * @param body body
   * @param jsonPath 解析表达式
   * @return 返回长度
   */
  public static int getBodyDataArrayCount(String body,String jsonPath){
    Object o = JsonPath.read(body,jsonPath);
    return Integer.parseInt(String.valueOf(o));
  }

  /**
   * HttpResponse类型的响应内容解析成字符串
   * @param responseBody 响应body
   * @return 解析后的字符串
   */
  public static String parseRespBodyToString(CloseableHttpResponse responseBody){

    try {
      return EntityUtils.toString(responseBody.getEntity(), Charset.forName("UTF-8"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String responseBodyToString(CloseableHttpResponse responseBody){

    String body;
    StringBuilder sb1 = new StringBuilder();
    InputStream in = null;

    try {
      body = EntityUtils.toString(responseBody.getEntity(), Charset.forName("UTF-8"));

      in = new ByteArrayInputStream(body.getBytes());

      byte[] bytes = new byte[4096];
      int size;
      while ((size = in.read(bytes)) > 0 ) {
        String str = new String(bytes,0,size,Charset.forName("UTF-8"));
        sb1.append(str);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    finally {
      try {
        in.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return sb1.toString();
  }

  /**
   * TestDataBean 类型对象的Json数据解析
   * @param testDataBean TestDataBean对象
   * @return
   */
  public static JSONObject strToJsonObject(TestDataBean testDataBean){

    try {
      return JSONObject.parseObject(testDataBean.getRequestData());
    } catch (Exception e) {
      e.printStackTrace();
      CheckPointUtils.myAssertFalse("["+testDataBean.getApiDescription()+"]用例的数据-json格式有问题");
    }
    return null;
  }

  /**
   * String 类型对象json数据解析
   * @param description 描述
   * @param strJson 字符串
   * @return 解析的数据
   */
  public static JSONObject strTOJsonObject(String description,String strJson){
    try {
      return JSONObject.parseObject(strJson);
    } catch (Exception e) {
      e.printStackTrace();
      CheckPointUtils.myAssertFalse("["+description+"]用例的数据-json格式有问题");
    }
    return null;
  }
}
