package com.example.util;

import lombok.extern.slf4j.Slf4j;
import org.testng.Reporter;

/**
 * @author 任鹏达
 * @version 1.0
 */

@Slf4j
public class MyLogger {


  /**
   * 接口请求数据日志
   * @param apiName 接口名称
   * @param url 接口地址
   * @param requestBody 请求body
   */
  public static void myRequestLogger(String apiName,String url,String requestBody){

    log.info("【"+apiName+"】接口--请求---->>>>>"+url+" "+requestBody);
    Reporter.log("【"+apiName+"】接口--请求---->>>>>"+url+" "+requestBody);
  }

  public static void myResponseLogger(String apiName,String responseBody){


    log.info("【"+apiName+"】接口:响应<<<<<-----"+responseBody);
    Reporter.log("【"+apiName+"】接口:响应<<<<<-----"+responseBody);
  }

  public static void myResponseLogger(String loggerInfo){

    log.info(loggerInfo);
    Reporter.log(loggerInfo);
  }
}
