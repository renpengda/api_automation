package com.example.util;

import com.example.bean.TestDataBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 任鹏达
 * @version 1.0
 */
@Slf4j
public class ApiRequestUtils {

  private static CloseableHttpClient httpClient;

  private CloseableHttpResponse response;

  private String responseBody;

  /**接口上下游关联数据**/
  public Map<String, Object> saveLinkedMap = new ConcurrentHashMap<>();

  /**正则表达式匹配规则**/
  private static final Pattern pattern = Pattern.compile("\\$\\{(.*?)\\}");

  static {
    PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
    manager.setMaxTotal(200);//连接池最大并发连接数
    manager.setDefaultMaxPerRoute(200);//单路由最大并发数，路由是对maxTotal的吸粉
    HttpClientConnectionManager connManager;
    httpClient = HttpClients.custom().setConnectionManager(manager).build();
  }

  private static RequestConfig config = RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(10000).setConnectTimeout(5000).setConnectionRequestTimeout(5000).build();

  /**
   * get请求
   *
   * @param testDataBean 实体类
   * @return 返回Json数据
   */
  public String doGet(TestDataBean testDataBean) {
    HttpGet httpGet = null;
    try {
      UrlEncodedFormEntity urlEncodedFormEntity = setEntity(parseMap(testDataBean.getRequestData()));
      if (urlEncodedFormEntity != null) {
        httpGet = new HttpGet(testDataBean.getUrl() + "?" + EntityUtils.toString(urlEncodedFormEntity, "UTF-8"));
      } else {
        httpGet = new HttpGet(testDataBean.getUrl());
      }
      log.info("请求地址为==========>>>" + testDataBean.getUrl() + "?" + EntityUtils.toString(urlEncodedFormEntity, "UTF-8"));
      httpGet.setConfig(config);
      response = httpClient.execute(httpGet);
      //将响应结果处理成Json格式
      responseBody = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
      log.info(testDataBean.getApiDescription() + "接口响应body<<<==========" + responseBody);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return responseBody;
  }

  /**
   * post请求
   * json格式数据
   *
   * @param testDataBean 实体类
   * @return 响应结果
   */
  public String doPost(TestDataBean testDataBean) {
    HttpPost httpPost = new HttpPost(testDataBean.getUrl());
    try {
      StringEntity entity = new StringEntity(testDataBean.getRequestData(), Charset.forName("UTF-8"));
      entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
      entity.setContentType("application/json");
      httpPost.setEntity(entity);
      httpPost.setConfig(config);
      response = httpClient.execute(httpPost);
      responseBody = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
      log.info(testDataBean.getApiDescription() + " 接口响应body<<<========== " + responseBody);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return responseBody;
  }

  /**
   * post kv请求数据
   *
   * @param testDataBean 实体类
   * @return 响应结果
   */
  public String doPostKV(TestDataBean testDataBean) {
    HttpPost httpPost = new HttpPost(testDataBean.getUrl());
    Map<String, Object> map = parseMap(testDataBean.getRequestData());
    UrlEncodedFormEntity entity;

    try {
      entity = setEntity(map);
      httpPost.setEntity(entity);
      httpPost.setConfig(config);
      responseBody = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
      log.info(testDataBean.getApiDescription() + " 接口响应body<<<========== " + responseBody);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return responseBody;
  }

  /**
   * put请求
   * json格式数据
   *
   * @param testDataBean 实体类
   * @return 响应结果
   */
  public String doPut(TestDataBean testDataBean) {
    HttpPut httpPut = new HttpPut(testDataBean.getUrl());
    try {
      StringEntity entity = new StringEntity(testDataBean.getRequestData(), Charset.forName("UTF-8"));
      entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
      entity.setContentType("application/json");
      httpPut.setEntity(entity);
      httpPut.setConfig(config);
      response = httpClient.execute(httpPut);
      responseBody = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
      log.info(testDataBean.getApiDescription() + " 接口响应body<<<========== " + responseBody);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return responseBody;
  }

  /**
   * delete请求
   * json格式数据
   *
   * @param testDataBean 实体类
   * @return 响应结果
   */
  public String doDelete(TestDataBean testDataBean) {
    HttpDelete httpDelete = null;
    try {
      UrlEncodedFormEntity urlEncodedFormEntity = setEntity(parseMap(testDataBean.getRequestData()));
      if (urlEncodedFormEntity != null) {
        httpDelete = new HttpDelete(testDataBean.getUrl() + "?" + EntityUtils.toString(urlEncodedFormEntity, "UTF-8"));
      } else {
        httpDelete = new HttpDelete(testDataBean.getUrl());
      }
      log.info("请求地址为==========>>>" + testDataBean.getUrl() + "?" + EntityUtils.toString(urlEncodedFormEntity, "UTF-8"));
      httpDelete.setConfig(config);
      response = httpClient.execute(httpDelete);
      //将响应结果处理成Json格式
      responseBody = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
      log.info(testDataBean.getApiDescription() + "接口响应body<<<==========" + responseBody);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return responseBody;
  }


  /**
   * 键值对请求参数解析成Map
   *
   * @param params 传入的请求参数例如："username=root，password=1234"
   * @return 返回一个Map
   */
  private Map<String, Object> parseMap(String params) {
    Map<String, Object> map = null;
    if (StringUtils.isNotBlank(params) && !"null".equalsIgnoreCase(params)) {
      map = new HashMap<>();
      String nameValue[] = params.split(",");
      for (String s : nameValue) {
        String value[] = s.split("=");
        map.put(value[0], value[1]);
      }
    }
    return map;
  }

  /**
   * form表单的map类型参数解析成实体
   *
   * @param map 表单数据
   * @return 实体内容
   * @throws Exception
   */
  private UrlEncodedFormEntity setEntity(Map<String, Object> map) throws Exception {

    List<BasicNameValuePair> formList = new ArrayList<>();
    for (Map.Entry<String, Object> entry : map.entrySet()) {
      formList.add(new BasicNameValuePair(entry.getKey(), (String) entry.getValue()));
    }
    log.info("传参数据==========>>>：" + formList.toString());
    return new UrlEncodedFormEntity(formList);

  }

  /**
   * @param body      响应body
   * @param relevancy json表达式，要存储的数据
   * @param apiName   接口名称
   * @return
   */
  public Map<String, Object> saveLinked(String body, String relevancy, String apiName) {
    if (StringUtils.isNotBlank(relevancy) && !"null".equalsIgnoreCase(relevancy)) {
      Map<String, Object> map = parseMap(relevancy);
      for (Map.Entry<String, Object> entry : map.entrySet()) {
        String key = entry.getKey();
        String jp = entry.getValue().toString();
        saveLinkedMap.put(key, JsonUtils.getJsonValueByJsonPath(body, jp, apiName));
      }
      return saveLinkedMap;
    }
    return saveLinkedMap;
  }


  /**
   * 替换接口关联数据
   *
   * @param requestData    请求数据 json表达式
   * @param apiDescription 接口描述
   * @return
   */
  public String patternLinked(String requestData, String apiDescription) {
    if (!StringUtils.isNotBlank(requestData)) {
      return requestData;
    }

    String newValue;
    Matcher matcher = pattern.matcher(requestData);
    while (matcher.find()) {

      System.out.println(matcher.group(0));
      System.out.println(matcher.group(1));
      System.out.println(matcher.group());

      String values = saveLinkedMap.get(matcher.group(1)).toString();

      newValue = requestData.replace(matcher.group(), values);
      requestData = newValue;
    }
    return requestData;
  }

}
